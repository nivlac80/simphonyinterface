﻿namespace SimphonyInterface
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tcSetup = new System.Windows.Forms.TabControl();
            this.tpTenderMedia = new System.Windows.Forms.TabPage();
            this.chkTenderMediaRVCInclude = new System.Windows.Forms.CheckBox();
            this.dgvTenderMedia = new System.Windows.Forms.DataGridView();
            this.tpRevenue = new System.Windows.Forms.TabPage();
            this.dgvRevenue = new System.Windows.Forms.DataGridView();
            this.tpDiscount = new System.Windows.Forms.TabPage();
            this.dgvDiscount = new System.Windows.Forms.DataGridView();
            this.tpTax = new System.Windows.Forms.TabPage();
            this.chkTaxAnalysisCodeActive = new System.Windows.Forms.CheckBox();
            this.cbbTaxAnalysisCode = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvTax = new System.Windows.Forms.DataGridView();
            this.tpServiceCharge = new System.Windows.Forms.TabPage();
            this.dgvServiceCharge = new System.Windows.Forms.DataGridView();
            this.tpFixedPeriod = new System.Windows.Forms.TabPage();
            this.cbbFixedPeriodAnalysisCode = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvFixedPeriod = new System.Windows.Forms.DataGridView();
            this.tpRevenueCenter = new System.Windows.Forms.TabPage();
            this.dgvRevenueCenter = new System.Windows.Forms.DataGridView();
            this.cbbRevenueCenterAnalysisCode = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tpMISC = new System.Windows.Forms.TabPage();
            this.txtCoverCountAccountCode = new System.Windows.Forms.TextBox();
            this.txtRoundingAccountCode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9.SuspendLayout();
            this.tcSetup.SuspendLayout();
            this.tpTenderMedia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTenderMedia)).BeginInit();
            this.tpRevenue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRevenue)).BeginInit();
            this.tpDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiscount)).BeginInit();
            this.tpTax.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTax)).BeginInit();
            this.tpServiceCharge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceCharge)).BeginInit();
            this.tpFixedPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFixedPeriod)).BeginInit();
            this.tpRevenueCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRevenueCenter)).BeginInit();
            this.tpMISC.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.Color.White;
            this.tabPage9.Controls.Add(this.btnCancel);
            this.tabPage9.Controls.Add(this.btnSave);
            this.tabPage9.Controls.Add(this.tcSetup);
            this.tabPage9.Location = new System.Drawing.Point(4, 25);
            this.tabPage9.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage9.Size = new System.Drawing.Size(949, 499);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Setup";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(837, 44);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(837, 8);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // tcSetup
            // 
            this.tcSetup.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tcSetup.Controls.Add(this.tpTenderMedia);
            this.tcSetup.Controls.Add(this.tpRevenue);
            this.tcSetup.Controls.Add(this.tpDiscount);
            this.tcSetup.Controls.Add(this.tpTax);
            this.tcSetup.Controls.Add(this.tpServiceCharge);
            this.tcSetup.Controls.Add(this.tpFixedPeriod);
            this.tcSetup.Controls.Add(this.tpRevenueCenter);
            this.tcSetup.Controls.Add(this.tpMISC);
            this.tcSetup.Location = new System.Drawing.Point(8, 8);
            this.tcSetup.Margin = new System.Windows.Forms.Padding(4);
            this.tcSetup.Multiline = true;
            this.tcSetup.Name = "tcSetup";
            this.tcSetup.SelectedIndex = 0;
            this.tcSetup.Size = new System.Drawing.Size(821, 483);
            this.tcSetup.TabIndex = 0;
            // 
            // tpTenderMedia
            // 
            this.tpTenderMedia.Controls.Add(this.chkTenderMediaRVCInclude);
            this.tpTenderMedia.Controls.Add(this.dgvTenderMedia);
            this.tpTenderMedia.Location = new System.Drawing.Point(4, 28);
            this.tpTenderMedia.Margin = new System.Windows.Forms.Padding(4);
            this.tpTenderMedia.Name = "tpTenderMedia";
            this.tpTenderMedia.Padding = new System.Windows.Forms.Padding(4);
            this.tpTenderMedia.Size = new System.Drawing.Size(813, 451);
            this.tpTenderMedia.TabIndex = 0;
            this.tpTenderMedia.Text = "T. MEDIA";
            this.tpTenderMedia.UseVisualStyleBackColor = true;
            // 
            // chkTenderMediaRVCInclude
            // 
            this.chkTenderMediaRVCInclude.AutoSize = true;
            this.chkTenderMediaRVCInclude.Location = new System.Drawing.Point(8, 8);
            this.chkTenderMediaRVCInclude.Margin = new System.Windows.Forms.Padding(4);
            this.chkTenderMediaRVCInclude.Name = "chkTenderMediaRVCInclude";
            this.chkTenderMediaRVCInclude.Size = new System.Drawing.Size(107, 21);
            this.chkTenderMediaRVCInclude.TabIndex = 0;
            this.chkTenderMediaRVCInclude.Text = "Include RVC";
            this.chkTenderMediaRVCInclude.UseVisualStyleBackColor = true;
            // 
            // dgvTenderMedia
            // 
            this.dgvTenderMedia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTenderMedia.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvTenderMedia.Location = new System.Drawing.Point(4, 37);
            this.dgvTenderMedia.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTenderMedia.Name = "dgvTenderMedia";
            this.dgvTenderMedia.Size = new System.Drawing.Size(805, 410);
            this.dgvTenderMedia.TabIndex = 1;
            // 
            // tpRevenue
            // 
            this.tpRevenue.Controls.Add(this.dgvRevenue);
            this.tpRevenue.Location = new System.Drawing.Point(4, 28);
            this.tpRevenue.Margin = new System.Windows.Forms.Padding(4);
            this.tpRevenue.Name = "tpRevenue";
            this.tpRevenue.Padding = new System.Windows.Forms.Padding(4);
            this.tpRevenue.Size = new System.Drawing.Size(813, 451);
            this.tpRevenue.TabIndex = 6;
            this.tpRevenue.Text = "REVENUE";
            this.tpRevenue.UseVisualStyleBackColor = true;
            // 
            // dgvRevenue
            // 
            this.dgvRevenue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRevenue.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvRevenue.Location = new System.Drawing.Point(4, 8);
            this.dgvRevenue.Margin = new System.Windows.Forms.Padding(4);
            this.dgvRevenue.Name = "dgvRevenue";
            this.dgvRevenue.Size = new System.Drawing.Size(805, 439);
            this.dgvRevenue.TabIndex = 0;
            // 
            // tpDiscount
            // 
            this.tpDiscount.Controls.Add(this.dgvDiscount);
            this.tpDiscount.Location = new System.Drawing.Point(4, 28);
            this.tpDiscount.Margin = new System.Windows.Forms.Padding(4);
            this.tpDiscount.Name = "tpDiscount";
            this.tpDiscount.Padding = new System.Windows.Forms.Padding(4);
            this.tpDiscount.Size = new System.Drawing.Size(813, 451);
            this.tpDiscount.TabIndex = 1;
            this.tpDiscount.Text = "DISCOUNT";
            this.tpDiscount.UseVisualStyleBackColor = true;
            // 
            // dgvDiscount
            // 
            this.dgvDiscount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDiscount.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvDiscount.Location = new System.Drawing.Point(4, 8);
            this.dgvDiscount.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDiscount.Name = "dgvDiscount";
            this.dgvDiscount.Size = new System.Drawing.Size(805, 439);
            this.dgvDiscount.TabIndex = 0;
            // 
            // tpTax
            // 
            this.tpTax.Controls.Add(this.chkTaxAnalysisCodeActive);
            this.tpTax.Controls.Add(this.cbbTaxAnalysisCode);
            this.tpTax.Controls.Add(this.label1);
            this.tpTax.Controls.Add(this.dgvTax);
            this.tpTax.Cursor = System.Windows.Forms.Cursors.Default;
            this.tpTax.Location = new System.Drawing.Point(4, 28);
            this.tpTax.Margin = new System.Windows.Forms.Padding(4);
            this.tpTax.Name = "tpTax";
            this.tpTax.Padding = new System.Windows.Forms.Padding(4);
            this.tpTax.Size = new System.Drawing.Size(813, 451);
            this.tpTax.TabIndex = 2;
            this.tpTax.Text = "TAX";
            this.tpTax.UseVisualStyleBackColor = true;
            // 
            // chkTaxAnalysisCodeActive
            // 
            this.chkTaxAnalysisCodeActive.AutoSize = true;
            this.chkTaxAnalysisCodeActive.Location = new System.Drawing.Point(315, 7);
            this.chkTaxAnalysisCodeActive.Margin = new System.Windows.Forms.Padding(4);
            this.chkTaxAnalysisCodeActive.Name = "chkTaxAnalysisCodeActive";
            this.chkTaxAnalysisCodeActive.Size = new System.Drawing.Size(68, 21);
            this.chkTaxAnalysisCodeActive.TabIndex = 5;
            this.chkTaxAnalysisCodeActive.Text = "Active";
            this.chkTaxAnalysisCodeActive.UseVisualStyleBackColor = true;
            this.chkTaxAnalysisCodeActive.CheckedChanged += new System.EventHandler(this.ChkTaxAnalysisCodeActive_CheckedChanged);
            // 
            // cbbTaxAnalysisCode
            // 
            this.cbbTaxAnalysisCode.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbTaxAnalysisCode.DisplayMember = "None";
            this.cbbTaxAnalysisCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTaxAnalysisCode.Enabled = false;
            this.cbbTaxAnalysisCode.FormattingEnabled = true;
            this.cbbTaxAnalysisCode.Location = new System.Drawing.Point(147, 5);
            this.cbbTaxAnalysisCode.Margin = new System.Windows.Forms.Padding(4);
            this.cbbTaxAnalysisCode.Name = "cbbTaxAnalysisCode";
            this.cbbTaxAnalysisCode.Size = new System.Drawing.Size(160, 24);
            this.cbbTaxAnalysisCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Analysis Code";
            // 
            // dgvTax
            // 
            this.dgvTax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTax.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvTax.Location = new System.Drawing.Point(4, 37);
            this.dgvTax.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTax.Name = "dgvTax";
            this.dgvTax.Size = new System.Drawing.Size(805, 410);
            this.dgvTax.TabIndex = 1;
            // 
            // tpServiceCharge
            // 
            this.tpServiceCharge.Controls.Add(this.dgvServiceCharge);
            this.tpServiceCharge.Location = new System.Drawing.Point(4, 28);
            this.tpServiceCharge.Margin = new System.Windows.Forms.Padding(4);
            this.tpServiceCharge.Name = "tpServiceCharge";
            this.tpServiceCharge.Padding = new System.Windows.Forms.Padding(4);
            this.tpServiceCharge.Size = new System.Drawing.Size(813, 451);
            this.tpServiceCharge.TabIndex = 3;
            this.tpServiceCharge.Text = "SER. CHARGE";
            this.tpServiceCharge.UseVisualStyleBackColor = true;
            // 
            // dgvServiceCharge
            // 
            this.dgvServiceCharge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServiceCharge.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvServiceCharge.Location = new System.Drawing.Point(4, 8);
            this.dgvServiceCharge.Margin = new System.Windows.Forms.Padding(4);
            this.dgvServiceCharge.Name = "dgvServiceCharge";
            this.dgvServiceCharge.Size = new System.Drawing.Size(805, 439);
            this.dgvServiceCharge.TabIndex = 0;
            // 
            // tpFixedPeriod
            // 
            this.tpFixedPeriod.Controls.Add(this.cbbFixedPeriodAnalysisCode);
            this.tpFixedPeriod.Controls.Add(this.label3);
            this.tpFixedPeriod.Controls.Add(this.dgvFixedPeriod);
            this.tpFixedPeriod.Location = new System.Drawing.Point(4, 28);
            this.tpFixedPeriod.Margin = new System.Windows.Forms.Padding(4);
            this.tpFixedPeriod.Name = "tpFixedPeriod";
            this.tpFixedPeriod.Padding = new System.Windows.Forms.Padding(4);
            this.tpFixedPeriod.Size = new System.Drawing.Size(813, 451);
            this.tpFixedPeriod.TabIndex = 4;
            this.tpFixedPeriod.Text = "F. PERIOD";
            this.tpFixedPeriod.UseVisualStyleBackColor = true;
            // 
            // cbbFixedPeriodAnalysisCode
            // 
            this.cbbFixedPeriodAnalysisCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbFixedPeriodAnalysisCode.FormattingEnabled = true;
            this.cbbFixedPeriodAnalysisCode.Location = new System.Drawing.Point(147, 5);
            this.cbbFixedPeriodAnalysisCode.Margin = new System.Windows.Forms.Padding(4);
            this.cbbFixedPeriodAnalysisCode.Name = "cbbFixedPeriodAnalysisCode";
            this.cbbFixedPeriodAnalysisCode.Size = new System.Drawing.Size(160, 24);
            this.cbbFixedPeriodAnalysisCode.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Analysis Code";
            // 
            // dgvFixedPeriod
            // 
            this.dgvFixedPeriod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFixedPeriod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvFixedPeriod.Location = new System.Drawing.Point(4, 37);
            this.dgvFixedPeriod.Margin = new System.Windows.Forms.Padding(4);
            this.dgvFixedPeriod.Name = "dgvFixedPeriod";
            this.dgvFixedPeriod.Size = new System.Drawing.Size(805, 410);
            this.dgvFixedPeriod.TabIndex = 1;
            // 
            // tpRevenueCenter
            // 
            this.tpRevenueCenter.Controls.Add(this.dgvRevenueCenter);
            this.tpRevenueCenter.Controls.Add(this.cbbRevenueCenterAnalysisCode);
            this.tpRevenueCenter.Controls.Add(this.label2);
            this.tpRevenueCenter.Location = new System.Drawing.Point(4, 28);
            this.tpRevenueCenter.Margin = new System.Windows.Forms.Padding(4);
            this.tpRevenueCenter.Name = "tpRevenueCenter";
            this.tpRevenueCenter.Padding = new System.Windows.Forms.Padding(4);
            this.tpRevenueCenter.Size = new System.Drawing.Size(813, 451);
            this.tpRevenueCenter.TabIndex = 5;
            this.tpRevenueCenter.Text = "REV. CENTER";
            this.tpRevenueCenter.UseVisualStyleBackColor = true;
            // 
            // dgvRevenueCenter
            // 
            this.dgvRevenueCenter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRevenueCenter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvRevenueCenter.Location = new System.Drawing.Point(4, 37);
            this.dgvRevenueCenter.Margin = new System.Windows.Forms.Padding(4);
            this.dgvRevenueCenter.Name = "dgvRevenueCenter";
            this.dgvRevenueCenter.Size = new System.Drawing.Size(805, 410);
            this.dgvRevenueCenter.TabIndex = 1;
            // 
            // cbbRevenueCenterAnalysisCode
            // 
            this.cbbRevenueCenterAnalysisCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbRevenueCenterAnalysisCode.FormattingEnabled = true;
            this.cbbRevenueCenterAnalysisCode.Location = new System.Drawing.Point(147, 5);
            this.cbbRevenueCenterAnalysisCode.Margin = new System.Windows.Forms.Padding(4);
            this.cbbRevenueCenterAnalysisCode.Name = "cbbRevenueCenterAnalysisCode";
            this.cbbRevenueCenterAnalysisCode.Size = new System.Drawing.Size(160, 24);
            this.cbbRevenueCenterAnalysisCode.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Analysis Code";
            // 
            // tpMISC
            // 
            this.tpMISC.Controls.Add(this.txtCoverCountAccountCode);
            this.tpMISC.Controls.Add(this.txtRoundingAccountCode);
            this.tpMISC.Controls.Add(this.label13);
            this.tpMISC.Controls.Add(this.label12);
            this.tpMISC.Location = new System.Drawing.Point(4, 28);
            this.tpMISC.Margin = new System.Windows.Forms.Padding(4);
            this.tpMISC.Name = "tpMISC";
            this.tpMISC.Padding = new System.Windows.Forms.Padding(4);
            this.tpMISC.Size = new System.Drawing.Size(813, 451);
            this.tpMISC.TabIndex = 7;
            this.tpMISC.Text = "MISC";
            this.tpMISC.UseVisualStyleBackColor = true;
            // 
            // txtCoverCountAccountCode
            // 
            this.txtCoverCountAccountCode.Location = new System.Drawing.Point(147, 35);
            this.txtCoverCountAccountCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtCoverCountAccountCode.Name = "txtCoverCountAccountCode";
            this.txtCoverCountAccountCode.Size = new System.Drawing.Size(132, 22);
            this.txtCoverCountAccountCode.TabIndex = 1;
            // 
            // txtRoundingAccountCode
            // 
            this.txtRoundingAccountCode.Location = new System.Drawing.Point(147, 5);
            this.txtRoundingAccountCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtRoundingAccountCode.Name = "txtRoundingAccountCode";
            this.txtRoundingAccountCode.Size = new System.Drawing.Size(132, 22);
            this.txtRoundingAccountCode.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 38);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 17);
            this.label13.TabIndex = 1;
            this.label13.Text = "Cover Count A/C";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Rounding A/C";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Location = new System.Drawing.Point(13, 13);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(957, 528);
            this.tabControl2.TabIndex = 2;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 551);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(990, 598);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(990, 598);
            this.Name = "frmSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            this.tabPage9.ResumeLayout(false);
            this.tcSetup.ResumeLayout(false);
            this.tpTenderMedia.ResumeLayout(false);
            this.tpTenderMedia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTenderMedia)).EndInit();
            this.tpRevenue.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRevenue)).EndInit();
            this.tpDiscount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDiscount)).EndInit();
            this.tpTax.ResumeLayout(false);
            this.tpTax.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTax)).EndInit();
            this.tpServiceCharge.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvServiceCharge)).EndInit();
            this.tpFixedPeriod.ResumeLayout(false);
            this.tpFixedPeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFixedPeriod)).EndInit();
            this.tpRevenueCenter.ResumeLayout(false);
            this.tpRevenueCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRevenueCenter)).EndInit();
            this.tpMISC.ResumeLayout(false);
            this.tpMISC.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabControl tcSetup;
        private System.Windows.Forms.TabPage tpTenderMedia;
        private System.Windows.Forms.CheckBox chkTenderMediaRVCInclude;
        private System.Windows.Forms.DataGridView dgvTenderMedia;
        private System.Windows.Forms.TabPage tpRevenue;
        private System.Windows.Forms.DataGridView dgvRevenue;
        private System.Windows.Forms.TabPage tpDiscount;
        private System.Windows.Forms.DataGridView dgvDiscount;
        private System.Windows.Forms.TabPage tpTax;
        private System.Windows.Forms.ComboBox cbbTaxAnalysisCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvTax;
        private System.Windows.Forms.TabPage tpServiceCharge;
        private System.Windows.Forms.DataGridView dgvServiceCharge;
        private System.Windows.Forms.TabPage tpFixedPeriod;
        private System.Windows.Forms.ComboBox cbbFixedPeriodAnalysisCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvFixedPeriod;
        private System.Windows.Forms.TabPage tpRevenueCenter;
        private System.Windows.Forms.DataGridView dgvRevenueCenter;
        private System.Windows.Forms.ComboBox cbbRevenueCenterAnalysisCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tpMISC;
        private System.Windows.Forms.TextBox txtCoverCountAccountCode;
        private System.Windows.Forms.TextBox txtRoundingAccountCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkTaxAnalysisCodeActive;
    }
}