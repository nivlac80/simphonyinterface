﻿using SQLLiteDB;
using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SimphonyInterface
{
    public partial class frmSettings : Form
    {
        private StringBuilder QueryStringBuilder = new StringBuilder();

        #region ConstraintQueryString

        private const string QueryTenderMedia = "SELECT TenderMedia AS [Tender Media],AccountCode AS [Account Code],Description FROM tblTenderMedia ORDER BY TenderMedia";
        private const string QueryRevenue = "SELECT MajorGroup AS [Major Group], AccountCode AS [Account Code], Description FROM tblRevenue ORDER BY MajorGroup";
        private const string QueryDiscount = "SELECT Discount, AccountCode AS [Account Code], Description FROM tblDiscount ORDER BY AccountCode";
        private const string QueryTax = "SELECT TaxID AS [Tax ID], AccountCode AS [Account Code], TaxCode AS [Tax Code], Description FROM tblTax ORDER BY TaxID"; 
        private const string QueryServiceCharge = "SELECT ServiceCharge AS [Service Charge], AccountCode AS [Account Code], Description FROM tblServiceCharge ORDER BY ServiceCharge";
        private const string QueryFixedPeriod = "SELECT FixedPeriodFrom AS [Fixed Period From], FixedPeriodTo AS [Fixed Period To], Q3ServingPeriod AS [Q3 Serving Period], Description FROM tblFixedPeriod ORDER BY FixedPeriodFrom";
        private const string QueryRevenueCenter = "SELECT RVC, Q3RVCCode AS [Q3 RVC Code], Description FROM tblRevenueCenter ORDER BY RVC";
        private const string QueryTenderMediaRVCInclude_and_MISC = "SELECT TenderMediaRVCInclude, TaxAnalysisCode, TaxAnalysisCodeActive, FixedPeriodAnalysisCode, RevenueCenterAnalysisCode, MISCRoundingAccountCode, MISCCoverCountAccountCode FROM tblTenderMediaRVCInclude_and_MISC";

        #endregion ConstraintQueryString

        private enum SettingType
        {
            TenderMedia,
            Revenue,
            Discount,
            Tax,
            ServiceCharge,
            FixedPeriod,
            RevenueCenter,
            TenderMediaRVCInclude_and_MISC,
            HistoryImportFilePath
        }

        public frmSettings()
        {
            InitializeComponent();

            this.dgvTenderMedia.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRevenue.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDiscount.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTax.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvServiceCharge.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFixedPeriod.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRevenueCenter.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void FrmSettings_Load(object sender, EventArgs e)
        {
            LoadComboBoxAnalysisCode();
            LoadData(SettingType.TenderMedia);
            LoadData(SettingType.Revenue);
            LoadData(SettingType.Discount);
            LoadData(SettingType.Tax);
            LoadData(SettingType.ServiceCharge);
            LoadData(SettingType.FixedPeriod);
            LoadData(SettingType.RevenueCenter);
            LoadData(SettingType.TenderMediaRVCInclude_and_MISC);
            LoadData(SettingType.HistoryImportFilePath);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (IsValidationPass() == false)
            {
                return;
            }

            string ErrMessage = string.Empty;
            QueryStringBuilder.Clear();

            Color RowDefaultColor = System.Drawing.Color.Empty;

            // tblTenderMedia
            QueryStringBuilder.AppendLine("DELETE FROM tblTenderMedia;");
            foreach (DataGridViewRow dgvRow in this.dgvTenderMedia.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _TenderMedia = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblTenderMedia(TenderMedia,AccountCode,Description) VALUES('" + _TenderMedia + "','" + _AccountCode + "','" + _Description + "');");
                }
            }

            // tblRevenue
            QueryStringBuilder.AppendLine("DELETE FROM tblRevenue;");
            foreach (DataGridViewRow dgvRow in this.dgvRevenue.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _MajorGroup = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblRevenue(MajorGroup,AccountCode,Description) VALUES('" + _MajorGroup + "','" + _AccountCode + "','" + _Description + "');");
                }
            }
            // tblDiscount
            QueryStringBuilder.AppendLine("DELETE FROM tblDiscount;");
            foreach (DataGridViewRow dgvRow in this.dgvDiscount.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _Discount = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblDiscount(Discount,AccountCode,Description) VALUES('" + _Discount + "','" + _AccountCode + "','" + _Description + "');");
                }
            }

            // tblTax
            QueryStringBuilder.AppendLine("DELETE FROM tblTax;");
            foreach (DataGridViewRow dgvRow in this.dgvTax.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _TaxID = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _TaxCode = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[3].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblTax(TaxID,AccountCode,TaxCode,Description) VALUES('" + _TaxID + "','" + _AccountCode + "','" + _TaxCode + "','" + _Description + "');");
                }
            }

            // tblServiceCharges
            QueryStringBuilder.AppendLine("DELETE FROM tblServiceCharge;");
            foreach (DataGridViewRow dgvRow in this.dgvServiceCharge.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _ServiceCharge = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblServiceCharge(ServiceCharge,AccountCode,Description) VALUES('" + _ServiceCharge + "','" + _AccountCode + "','" + _Description + "');");
                }
            }

            // tblFixedPeriod
            QueryStringBuilder.AppendLine("DELETE FROM tblFixedPeriod;");
            foreach (DataGridViewRow dgvRow in this.dgvFixedPeriod.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _FixedPeriodFrom = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _FixedPeriodTo = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Q3ServingPeriod = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[3].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblFixedPeriod(FixedPeriodFrom,FixedPeriodTo,Q3ServingPeriod,Description) VALUES('" + _FixedPeriodFrom + "','" + _FixedPeriodTo + "','" + _Q3ServingPeriod + "','" + _Description + "');");
                }
            }

            // tblRevenueCenter
            QueryStringBuilder.AppendLine("DELETE FROM tblRevenueCenter;");
            foreach (DataGridViewRow dgvRow in this.dgvRevenueCenter.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    dgvRow.DefaultCellStyle.BackColor = RowDefaultColor;
                    string _RVC = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _Q3RVCCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Description = dgvRow.Cells[2].Value.ToString().Replace("'", "''").Trim();
                    QueryStringBuilder.AppendLine("INSERT INTO tblRevenueCenter(RVC,Q3RVCCode,Description) VALUES('" + _RVC + "','" + _Q3RVCCode + "','" + _Description + "');");
                }
            }

            // tblTenderMediaRVCInclude_and_MISC
            QueryStringBuilder.AppendLine("DELETE FROM tblTenderMediaRVCInclude_and_MISC;");
            int _TenderMediaRVCInclude = this.chkTenderMediaRVCInclude.Checked == true ? 1 : 0;
            int _TaxAnalysisCode = Convert.ToInt32(this.cbbTaxAnalysisCode.SelectedIndex);
            int _FixedPeriodAnalysisCode = Convert.ToInt32(this.cbbFixedPeriodAnalysisCode.SelectedIndex);
            int _RevenueCenterAnalysisCode = Convert.ToInt32(this.cbbRevenueCenterAnalysisCode.SelectedIndex);
            int _TaxAnalysisCodeActive = this.chkTaxAnalysisCodeActive.Checked == true ? 1 : 0;
            txtRoundingAccountCode.BackColor = RowDefaultColor;
            txtCoverCountAccountCode.BackColor = RowDefaultColor;
            string _MISCRoundingAccountCode = txtRoundingAccountCode.Text.Trim();
            string _MISCCoverCountAccountCode = txtCoverCountAccountCode.Text.Trim();
            QueryStringBuilder.AppendLine("INSERT INTO tblTenderMediaRVCInclude_and_MISC(TenderMediaRVCInclude,TaxAnalysisCode,TaxAnalysisCodeActive,FixedPeriodAnalysisCode,RevenueCenterAnalysisCode,MISCRoundingAccountCode,MISCCoverCountAccountCode) VALUES(" + _TenderMediaRVCInclude + "," + _TaxAnalysisCode + "," + _TaxAnalysisCodeActive + "," + _FixedPeriodAnalysisCode + "," + _RevenueCenterAnalysisCode + ",'" + _MISCRoundingAccountCode + "','" + _MISCCoverCountAccountCode + "');");

            int _AffectedRow = DBConnector.ExecuteQuery(QueryStringBuilder.ToString());

            this.Close();
        }

        private void LoadData(SettingType st)
        {
            switch (st)
            {
                case SettingType.TenderMedia:
                    DataTable dtTenderMedia = DBConnector.ReadTable(QueryTenderMedia);
                    this.dgvTenderMedia.DataSource = dtTenderMedia;
                    break;

                case SettingType.Revenue:
                    DataTable dtRevenue = DBConnector.ReadTable(QueryRevenue);
                    this.dgvRevenue.DataSource = dtRevenue;
                    break;

                case SettingType.Discount:
                    DataTable tblDiscount = DBConnector.ReadTable(QueryDiscount);
                    this.dgvDiscount.DataSource = tblDiscount;
                    break;

                case SettingType.Tax:
                    DataTable tblTax = DBConnector.ReadTable(QueryTax);
                    this.dgvTax.DataSource = tblTax;
                    break;

                case SettingType.ServiceCharge:
                    DataTable tblServiceCharge = DBConnector.ReadTable(QueryServiceCharge);
                    this.dgvServiceCharge.DataSource = tblServiceCharge;
                    break;

                case SettingType.FixedPeriod:
                    DataTable tblFixedPeriod = DBConnector.ReadTable(QueryFixedPeriod);
                    this.dgvFixedPeriod.DataSource = tblFixedPeriod;
                    break;

                case SettingType.RevenueCenter:
                    DataTable tblRevenueCenter = DBConnector.ReadTable(QueryRevenueCenter);
                    this.dgvRevenueCenter.DataSource = tblRevenueCenter;
                    break;

                case SettingType.TenderMediaRVCInclude_and_MISC:
                    DataTable tblTenderMediaRVCInclude_and_MISC = DBConnector.ReadTable(QueryTenderMediaRVCInclude_and_MISC);
                    this.chkTenderMediaRVCInclude.Checked = Convert.ToBoolean(tblTenderMediaRVCInclude_and_MISC.Rows[0]["TenderMediaRVCInclude"]);
                    this.cbbTaxAnalysisCode.SelectedIndex = Convert.ToInt32(tblTenderMediaRVCInclude_and_MISC.Rows[0]["TaxAnalysisCode"]);
                    this.chkTaxAnalysisCodeActive.Checked = Convert.ToBoolean(tblTenderMediaRVCInclude_and_MISC.Rows[0]["TaxAnalysisCodeActive"]);
                    this.cbbFixedPeriodAnalysisCode.SelectedIndex = Convert.ToInt32(tblTenderMediaRVCInclude_and_MISC.Rows[0]["FixedPeriodAnalysisCode"]);
                    this.cbbRevenueCenterAnalysisCode.SelectedIndex = Convert.ToInt32(tblTenderMediaRVCInclude_and_MISC.Rows[0]["RevenueCenterAnalysisCode"]);
                    this.txtRoundingAccountCode.Text = tblTenderMediaRVCInclude_and_MISC.Rows[0]["MISCRoundingAccountCode"].ToString();
                    this.txtCoverCountAccountCode.Text = tblTenderMediaRVCInclude_and_MISC.Rows[0]["MISCCoverCountAccountCode"].ToString();
                    break;

                default:
                    break;
            }
        }

        private void LoadComboBoxAnalysisCode()
        {
            ComboboxItem AnalysisCode1 = new ComboboxItem();
            AnalysisCode1.Text = "Analysis Code 1";
            AnalysisCode1.Value = 1;
            ComboboxItem AnalysisCode2 = new ComboboxItem();
            AnalysisCode2.Text = "Analysis Code 2";
            AnalysisCode2.Value = 2;
            ComboboxItem AnalysisCode3 = new ComboboxItem();
            AnalysisCode3.Text = "Analysis Code 3";
            AnalysisCode3.Value = 3;
            ComboboxItem AnalysisCode4 = new ComboboxItem();
            AnalysisCode4.Text = "Analysis Code 4";
            AnalysisCode4.Value = 4;
            ComboboxItem AnalysisCode5 = new ComboboxItem();
            AnalysisCode5.Text = "Analysis Code 5";
            AnalysisCode5.Value = 5;
            ComboboxItem AnalysisCode6 = new ComboboxItem();
            AnalysisCode6.Text = "Analysis Code 6";
            AnalysisCode6.Value = 6;
            ComboboxItem AnalysisCode7 = new ComboboxItem();
            AnalysisCode7.Text = "Analysis Code 7";
            AnalysisCode7.Value = 7;
            ComboboxItem AnalysisCode8 = new ComboboxItem();
            AnalysisCode8.Text = "Analysis Code 8";
            AnalysisCode8.Value = 8;
            ComboboxItem AnalysisCode9 = new ComboboxItem();
            AnalysisCode9.Text = "Analysis Code 9";
            AnalysisCode9.Value = 9;
            ComboboxItem AnalysisCode10 = new ComboboxItem();
            AnalysisCode10.Text = "Analysis Code 10";
            AnalysisCode10.Value = 10;

            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode1);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode2);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode3);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode4);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode5);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode6);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode7);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode8);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode9);
            this.cbbTaxAnalysisCode.Items.Add(AnalysisCode10);
            this.cbbTaxAnalysisCode.SelectedIndex = 0;

            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode1);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode2);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode3);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode4);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode5);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode6);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode7);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode8);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode9);
            this.cbbFixedPeriodAnalysisCode.Items.Add(AnalysisCode10);
            this.cbbFixedPeriodAnalysisCode.SelectedIndex = 0;

            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode1);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode2);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode3);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode4);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode5);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode6);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode7);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode8);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode9);
            this.cbbRevenueCenterAnalysisCode.Items.Add(AnalysisCode10);
            this.cbbRevenueCenterAnalysisCode.SelectedIndex = 0;
        }

        private bool IsValidationPass()
        {
            string _ErrMessage = "Cannot set same Analysis Code between Tax,Fixed Period & Revenue Center settings";

            //First Step : Check all selected Analysis Code must unique
            if (this.chkTaxAnalysisCodeActive.Checked == true)
            {
                if ((this.cbbTaxAnalysisCode.SelectedIndex == this.cbbFixedPeriodAnalysisCode.SelectedIndex) || (this.cbbTaxAnalysisCode.SelectedIndex == this.cbbRevenueCenterAnalysisCode.SelectedIndex) || (this.cbbFixedPeriodAnalysisCode.SelectedIndex == this.cbbRevenueCenterAnalysisCode.SelectedIndex))
                {
                    MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
            else
            {
                if (this.cbbFixedPeriodAnalysisCode.SelectedIndex == this.cbbRevenueCenterAnalysisCode.SelectedIndex)
                {
                    MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }

            _ErrMessage = "Please do not leave empty on important fields";
            bool _IsValidationPass = true;

            // tblTenderMedia
            foreach (DataGridViewRow dgvRow in this.dgvTenderMedia.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _TenderMedia = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_TenderMedia == string.Empty || _AccountCode == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }
                }
            }
            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[0];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            // tblRevenue
            foreach (DataGridViewRow dgvRow in this.dgvRevenue.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _MajorGroup = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_MajorGroup == string.Empty || _AccountCode == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }
                }
            }
            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[1];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            // tblDiscount
            foreach (DataGridViewRow dgvRow in this.dgvDiscount.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _Discount = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_Discount == string.Empty || _AccountCode == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }
                }
            }
            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[2];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            // tblTax
            foreach (DataGridViewRow dgvRow in this.dgvTax.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _TaxID = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _TaxCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_TaxID == string.Empty || _TaxCode == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }
                }
            }
            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[3];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            // tblServiceCharges
            foreach (DataGridViewRow dgvRow in this.dgvServiceCharge.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _ServiceCharge = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _AccountCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_ServiceCharge == string.Empty || _AccountCode == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }
                }
            }
            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[4];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            // tblFixedPeriod
            int x = 0;
            int FixedPeriodFrom_Previous;
            int FixedPeriodTo_Previous;
            foreach (DataGridViewRow dgvRow in this.dgvFixedPeriod.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _FixedPeriodFrom = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _FixedPeriodTo = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();
                    string _Q3ServingPeriod = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_FixedPeriodFrom == string.Empty || _FixedPeriodTo == string.Empty || _Q3ServingPeriod == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }

                    int FixedPeriodFrom_Current;
                    if (!int.TryParse(_FixedPeriodFrom, out FixedPeriodFrom_Current))
                    {
                        TabPage t = tcSetup.TabPages[5];
                        tcSetup.SelectedTab = t;
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        MessageBox.Show("Input numeric on Fixed Period From / To columns only", "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }

                    int FixedPeriodTo_Current;
                    if (!int.TryParse(_FixedPeriodTo, out FixedPeriodTo_Current))
                    {
                        TabPage t = tcSetup.TabPages[5];
                        tcSetup.SelectedTab = t;
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        MessageBox.Show("Input numeric on Fixed Period From / To columns only", "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }

                    if (FixedPeriodFrom_Current >= FixedPeriodTo_Current)
                    {
                        TabPage t = tcSetup.TabPages[5];
                        tcSetup.SelectedTab = t;
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        MessageBox.Show("Fixed Period To must greather than Fixed Period From", "Setup > Invalid Fixed Period To", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return false;
                    }

                    if (x > 0)
                    {
                        FixedPeriodFrom_Previous = Convert.ToInt32(this.dgvFixedPeriod.Rows[x - 1].Cells[0].Value);
                        FixedPeriodTo_Previous = Convert.ToInt32(this.dgvFixedPeriod.Rows[x - 1].Cells[1].Value);

                        if (FixedPeriodFrom_Previous >= FixedPeriodFrom_Current)
                        {
                            TabPage t = tcSetup.TabPages[5];
                            tcSetup.SelectedTab = t;
                            dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                            MessageBox.Show("Please set Fixed Period From value in ascending order", "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }

                        if (FixedPeriodTo_Previous >= FixedPeriodFrom_Current)
                        {
                            TabPage t = tcSetup.TabPages[5];
                            tcSetup.SelectedTab = t;
                            dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                            MessageBox.Show("Overlap hour found on Fixed Period From", "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }
                    x++;
                }
            }

            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[5];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            // tblRevenueCenter
            foreach (DataGridViewRow dgvRow in this.dgvRevenueCenter.Rows)
            {
                if (dgvRow.DataBoundItem != null)
                {
                    string _RVC = dgvRow.Cells[0].Value.ToString().Replace("'", "''").Trim();
                    string _Q3RVCCode = dgvRow.Cells[1].Value.ToString().Replace("'", "''").Trim();

                    if (_RVC == string.Empty || _Q3RVCCode == string.Empty)
                    {
                        dgvRow.DefaultCellStyle.BackColor = Color.LightYellow;
                        _IsValidationPass = false;
                    }
                }
            }
            if (_IsValidationPass == false)
            {
                TabPage t = tcSetup.TabPages[6];
                tcSetup.SelectedTab = t;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            /* Comment because is not compulsory fields
            // tblMISC
            string _MISCRoundingAccountCode = txtRoundingAccountCode.Text.Trim();
            string _MISCCoverCountAccountCode = txtCoverCountAccountCode.Text.Trim();
            if (_MISCRoundingAccountCode == string.Empty || _MISCCoverCountAccountCode == string.Empty)
            {
                TabPage t = tcSetup.TabPages[7];
                tcSetup.SelectedTab = t;
                this.txtRoundingAccountCode.BackColor = this.txtRoundingAccountCode.Text.Trim() == string.Empty ? Color.LightYellow : SystemColors.AppWorkspace;
                this.txtCoverCountAccountCode.BackColor = this.txtCoverCountAccountCode.Text.Trim() == string.Empty ? Color.LightYellow : SystemColors.AppWorkspace;
                MessageBox.Show(_ErrMessage, "Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                _IsValidationPass = false;
            }
            */

            return _IsValidationPass;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChkTaxAnalysisCodeActive_CheckedChanged(object sender, EventArgs e)
        {
            this.cbbTaxAnalysisCode.Enabled= this.chkTaxAnalysisCodeActive.Checked;
        }
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}