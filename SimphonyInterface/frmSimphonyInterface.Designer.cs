﻿namespace SimphonyInterface
{
    partial class frmSimphonyInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImportFP = new System.Windows.Forms.Button();
            this.btnImportGL = new System.Windows.Forms.Button();
            this.txtImportFP = new System.Windows.Forms.TextBox();
            this.txtImportGL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnImport = new System.Windows.Forms.Button();
            this.mstMain = new System.Windows.Forms.MenuStrip();
            this.tsmiTools = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.txtTotalCoverCount = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTotalVariance = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTotalRounding = new System.Windows.Forms.TextBox();
            this.txtTotalServiceCharge = new System.Windows.Forms.TextBox();
            this.txtTotalTax = new System.Windows.Forms.TextBox();
            this.txtTotalDiscount = new System.Windows.Forms.TextBox();
            this.txtTotalRevenue = new System.Windows.Forms.TextBox();
            this.txtTotalTenderMedia = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7FPMI = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mstMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnImportFP
            // 
            this.btnImportFP.Location = new System.Drawing.Point(596, 66);
            this.btnImportFP.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportFP.Name = "btnImportFP";
            this.btnImportFP.Size = new System.Drawing.Size(45, 28);
            this.btnImportFP.TabIndex = 1;
            this.btnImportFP.Text = "...";
            this.btnImportFP.UseVisualStyleBackColor = true;
            this.btnImportFP.Click += new System.EventHandler(this.BtnImportFP_Click);
            // 
            // btnImportGL
            // 
            this.btnImportGL.Location = new System.Drawing.Point(596, 36);
            this.btnImportGL.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportGL.Name = "btnImportGL";
            this.btnImportGL.Size = new System.Drawing.Size(45, 28);
            this.btnImportGL.TabIndex = 0;
            this.btnImportGL.Text = "...";
            this.btnImportGL.UseVisualStyleBackColor = true;
            this.btnImportGL.Click += new System.EventHandler(this.BtnImportGL_Click);
            // 
            // txtImportFP
            // 
            this.txtImportFP.Location = new System.Drawing.Point(119, 67);
            this.txtImportFP.Margin = new System.Windows.Forms.Padding(4);
            this.txtImportFP.Name = "txtImportFP";
            this.txtImportFP.ReadOnly = true;
            this.txtImportFP.Size = new System.Drawing.Size(471, 22);
            this.txtImportFP.TabIndex = 4;
            this.txtImportFP.TabStop = false;
            // 
            // txtImportGL
            // 
            this.txtImportGL.Location = new System.Drawing.Point(119, 37);
            this.txtImportGL.Margin = new System.Windows.Forms.Padding(4);
            this.txtImportGL.Name = "txtImportGL";
            this.txtImportGL.ReadOnly = true;
            this.txtImportGL.Size = new System.Drawing.Size(471, 22);
            this.txtImportGL.TabIndex = 3;
            this.txtImportGL.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "FP Import File";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "GL Import File";
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(649, 67);
            this.btnImport.Margin = new System.Windows.Forms.Padding(4);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(100, 28);
            this.btnImport.TabIndex = 2;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // mstMain
            // 
            this.mstMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiTools,
            this.tsmiAbout});
            this.mstMain.Location = new System.Drawing.Point(0, 0);
            this.mstMain.Name = "mstMain";
            this.mstMain.Size = new System.Drawing.Size(757, 28);
            this.mstMain.TabIndex = 2;
            // 
            // tsmiTools
            // 
            this.tsmiTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSettings,
            this.tsmiExit});
            this.tsmiTools.Name = "tsmiTools";
            this.tsmiTools.Size = new System.Drawing.Size(58, 24);
            this.tsmiTools.Text = "&Tools";
            // 
            // tsmiSettings
            // 
            this.tsmiSettings.Name = "tsmiSettings";
            this.tsmiSettings.Size = new System.Drawing.Size(224, 26);
            this.tsmiSettings.Text = "&Settings";
            this.tsmiSettings.Click += new System.EventHandler(this.TsmiSettings_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(224, 26);
            this.tsmiExit.Text = "E&xit";
            this.tsmiExit.Click += new System.EventHandler(this.TsmiExit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExport);
            this.groupBox1.Controls.Add(this.txtTotalCoverCount);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtTotalVariance);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtTotalRounding);
            this.groupBox1.Controls.Add(this.txtTotalServiceCharge);
            this.groupBox1.Controls.Add(this.txtTotalTax);
            this.groupBox1.Controls.Add(this.txtTotalDiscount);
            this.groupBox1.Controls.Add(this.txtTotalRevenue);
            this.groupBox1.Controls.Add(this.txtTotalTenderMedia);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7FPMI);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(16, 102);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(733, 294);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Summary";
            // 
            // btnExport
            // 
            this.btnExport.Enabled = false;
            this.btnExport.Location = new System.Drawing.Point(215, 252);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(100, 28);
            this.btnExport.TabIndex = 0;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.BtnExport_Click);
            // 
            // txtTotalCoverCount
            // 
            this.txtTotalCoverCount.Location = new System.Drawing.Point(215, 222);
            this.txtTotalCoverCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalCoverCount.Name = "txtTotalCoverCount";
            this.txtTotalCoverCount.ReadOnly = true;
            this.txtTotalCoverCount.Size = new System.Drawing.Size(132, 22);
            this.txtTotalCoverCount.TabIndex = 15;
            this.txtTotalCoverCount.TabStop = false;
            this.txtTotalCoverCount.Text = "0.00";
            this.txtTotalCoverCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(19, 229);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 17);
            this.label15.TabIndex = 14;
            this.label15.Text = "Cover Count";
            // 
            // txtTotalVariance
            // 
            this.txtTotalVariance.Location = new System.Drawing.Point(215, 192);
            this.txtTotalVariance.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalVariance.Name = "txtTotalVariance";
            this.txtTotalVariance.ReadOnly = true;
            this.txtTotalVariance.Size = new System.Drawing.Size(132, 22);
            this.txtTotalVariance.TabIndex = 13;
            this.txtTotalVariance.TabStop = false;
            this.txtTotalVariance.Text = "0.00";
            this.txtTotalVariance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 201);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 17);
            this.label14.TabIndex = 12;
            this.label14.Text = "Variance";
            // 
            // txtTotalRounding
            // 
            this.txtTotalRounding.Location = new System.Drawing.Point(215, 164);
            this.txtTotalRounding.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalRounding.Name = "txtTotalRounding";
            this.txtTotalRounding.ReadOnly = true;
            this.txtTotalRounding.Size = new System.Drawing.Size(132, 22);
            this.txtTotalRounding.TabIndex = 11;
            this.txtTotalRounding.TabStop = false;
            this.txtTotalRounding.Text = "0.00";
            this.txtTotalRounding.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalServiceCharge
            // 
            this.txtTotalServiceCharge.Location = new System.Drawing.Point(215, 136);
            this.txtTotalServiceCharge.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalServiceCharge.Name = "txtTotalServiceCharge";
            this.txtTotalServiceCharge.ReadOnly = true;
            this.txtTotalServiceCharge.Size = new System.Drawing.Size(132, 22);
            this.txtTotalServiceCharge.TabIndex = 10;
            this.txtTotalServiceCharge.TabStop = false;
            this.txtTotalServiceCharge.Text = "0.00";
            this.txtTotalServiceCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalTax
            // 
            this.txtTotalTax.Location = new System.Drawing.Point(215, 108);
            this.txtTotalTax.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalTax.Name = "txtTotalTax";
            this.txtTotalTax.ReadOnly = true;
            this.txtTotalTax.Size = new System.Drawing.Size(132, 22);
            this.txtTotalTax.TabIndex = 9;
            this.txtTotalTax.TabStop = false;
            this.txtTotalTax.Text = "0.00";
            this.txtTotalTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalDiscount
            // 
            this.txtTotalDiscount.Location = new System.Drawing.Point(215, 79);
            this.txtTotalDiscount.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalDiscount.Name = "txtTotalDiscount";
            this.txtTotalDiscount.ReadOnly = true;
            this.txtTotalDiscount.Size = new System.Drawing.Size(132, 22);
            this.txtTotalDiscount.TabIndex = 8;
            this.txtTotalDiscount.TabStop = false;
            this.txtTotalDiscount.Text = "0.00";
            this.txtTotalDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalRevenue
            // 
            this.txtTotalRevenue.Location = new System.Drawing.Point(215, 51);
            this.txtTotalRevenue.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalRevenue.Name = "txtTotalRevenue";
            this.txtTotalRevenue.ReadOnly = true;
            this.txtTotalRevenue.Size = new System.Drawing.Size(132, 22);
            this.txtTotalRevenue.TabIndex = 7;
            this.txtTotalRevenue.TabStop = false;
            this.txtTotalRevenue.Text = "0.00";
            this.txtTotalRevenue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalTenderMedia
            // 
            this.txtTotalTenderMedia.Location = new System.Drawing.Point(215, 23);
            this.txtTotalTenderMedia.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalTenderMedia.Name = "txtTotalTenderMedia";
            this.txtTotalTenderMedia.ReadOnly = true;
            this.txtTotalTenderMedia.Size = new System.Drawing.Size(132, 22);
            this.txtTotalTenderMedia.TabIndex = 6;
            this.txtTotalTenderMedia.TabStop = false;
            this.txtTotalTenderMedia.Text = "0.00";
            this.txtTotalTenderMedia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 174);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(105, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "Total Rounding";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 146);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "Total Service Charge";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 90);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Total Discount";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 118);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Total Tax";
            // 
            // label7FPMI
            // 
            this.label7FPMI.AutoSize = true;
            this.label7FPMI.Location = new System.Drawing.Point(19, 62);
            this.label7FPMI.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7FPMI.Name = "label7FPMI";
            this.label7FPMI.Size = new System.Drawing.Size(101, 17);
            this.label7FPMI.TabIndex = 1;
            this.label7FPMI.Text = "Total Revenue";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 33);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Total Tender Media";
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(64, 24);
            this.tsmiAbout.Text = "&About";
            this.tsmiAbout.Click += new System.EventHandler(this.tsmiAbout_Click);
            // 
            // frmSimphonyInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 405);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnImportFP);
            this.Controls.Add(this.mstMain);
            this.Controls.Add(this.btnImportGL);
            this.Controls.Add(this.txtImportGL);
            this.Controls.Add(this.txtImportFP);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(775, 452);
            this.MainMenuStrip = this.mstMain;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(775, 452);
            this.MinimizeBox = false;
            this.Name = "frmSimphonyInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simphony Interface";
            this.Load += new System.EventHandler(this.FrmSimphonyInterface_Load);
            this.mstMain.ResumeLayout(false);
            this.mstMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnImportFP;
        private System.Windows.Forms.Button btnImportGL;
        private System.Windows.Forms.TextBox txtImportFP;
        private System.Windows.Forms.TextBox txtImportGL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.MenuStrip mstMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiTools;
        private System.Windows.Forms.ToolStripMenuItem tsmiSettings;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.TextBox txtTotalCoverCount;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTotalVariance;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTotalRounding;
        private System.Windows.Forms.TextBox txtTotalServiceCharge;
        private System.Windows.Forms.TextBox txtTotalTax;
        private System.Windows.Forms.TextBox txtTotalDiscount;
        private System.Windows.Forms.TextBox txtTotalRevenue;
        private System.Windows.Forms.TextBox txtTotalTenderMedia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7FPMI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
    }
}

