﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace SimphonyInterface
{
    public partial class frmAbout : Form
    {
        DateTime _Datevalue;
        public frmAbout(DateTime Datevalue)
        {
            InitializeComponent();
            _Datevalue = Datevalue;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            var appSettings = ConfigurationManager.AppSettings;
            string strExpiry = appSettings["ExpiryDate"].ToString();
            string date_str = _Datevalue.ToString("dd/MM/yyyy");
            lblExpiry.Text = "Expiration at : " + date_str;
        }
    }
}
