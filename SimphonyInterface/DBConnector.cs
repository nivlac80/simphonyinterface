﻿using System;
using System.Data;
using System.Data.SQLite;

namespace SQLLiteDB
{
    internal class DBConnector
    {
        public static DataTable ReadTable(string selectQuery)
        {
            DataSet sqlite_Dataset = new DataSet();
            DataTable sqlite_DT = new DataTable();

            SQLiteConnection sqlite_con = new SQLiteConnection("Data Source=database.db; Version = 3; New = True; Compress = True; ");
            sqlite_con.Open();
            //SQLiteCommand sqlite_cmd = sqlite_con.CreateCommand();
            SQLiteDataAdapter sqlite_Adapter = new SQLiteDataAdapter(selectQuery, sqlite_con);
            sqlite_Dataset.Reset();
            sqlite_Adapter.Fill(sqlite_Dataset);
            sqlite_DT = sqlite_Dataset.Tables[0];
            sqlite_con.Close();

            return sqlite_DT;
        }

        public static int ExecuteQuery(string cmdQuery)
        {
            SQLiteConnection sqlite_con = new SQLiteConnection("Data Source=database.db; Version = 3; New = True; Compress = True; ");
            sqlite_con.Open();
            SQLiteCommand sqlite_cmd = sqlite_con.CreateCommand();
            sqlite_cmd.CommandText = cmdQuery;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_con.Close();

            int _RowAffected = 0;
            DataTable dtRowAffected = ReadTable("SELECT changes()");
            _RowAffected = Convert.ToInt32(dtRowAffected.Rows[0][0]);

            return _RowAffected;
        }

        public static int RowAffected()
        {
            int _RowAffected = 0;
            DataTable dtRowAffected = ReadTable("SELECT changes()");
            _RowAffected = Convert.ToInt32(dtRowAffected.Rows[0][0]);

            return _RowAffected;
        }
    }
}