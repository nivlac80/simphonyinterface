﻿using SQLLiteDB;
using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimphonyInterface
{
    public partial class frmSimphonyInterface : Form
    {
        DateTime dtExpiry = new DateTime(2020, 07, 31);
        private string QueryString;
        private StringBuilder QueryStringBuilder = new StringBuilder();
        private DataTable dtImportSource = new DataTable();

        #region ConstraintQueryString

        private const string QueryTenderMedia = "SELECT TenderMedia AS [Tender Media],AccountCode AS [Account Code],Description FROM tblTenderMedia ORDER BY TenderMedia";
        private const string QueryRevenue = "SELECT MajorGroup AS [Major Group], AccountCode AS [Account Code], Description FROM tblRevenue ORDER BY MajorGroup";
        private const string QueryDiscount = "SELECT Discount, AccountCode AS [Account Code], Description FROM tblDiscount ORDER BY AccountCode";
        private const string QueryTax = "SELECT TaxID AS [Tax ID], AccountCode AS [Account Code], TaxCode AS [Tax Code], Description FROM tblTax ORDER BY TaxID";
        private const string QueryServiceCharge = "SELECT ServiceCharge AS [Service Charge], AccountCode AS [Account Code], Description FROM tblServiceCharge ORDER BY ServiceCharge";
        private const string QueryFixedPeriod = "SELECT FixedPeriodFrom AS [Fixed Period From], FixedPeriodTo AS [Fixed Period To], Q3ServingPeriod AS [Q3 Serving Period], Description FROM tblFixedPeriod ORDER BY FixedPeriodFrom";
        private const string QueryRevenueCenter = "SELECT RVC, Q3RVCCode AS [Q3 RVC Code], Description FROM tblRevenueCenter ORDER BY RVC";
        private const string QueryTenderMediaRVCInclude_and_MISC = "SELECT TenderMediaRVCInclude, TaxAnalysisCode, TaxAnalysisCodeActive, FixedPeriodAnalysisCode, RevenueCenterAnalysisCode, MISCRoundingAccountCode, MISCCoverCountAccountCode FROM tblTenderMediaRVCInclude_and_MISC";

        #endregion ConstraintQueryString

        public frmSimphonyInterface()
        {
            InitializeComponent();

            dtImportSource.TableName = "dtImportSource";
            dtImportSource.Columns.Add("Type", typeof(String));
            dtImportSource.Columns.Add("Code", typeof(String));
            dtImportSource.Columns.Add("Mapping", typeof(String));
            dtImportSource.Columns.Add("Value", typeof(Decimal));
            dtImportSource.Columns.Add("AdditionalValue", typeof(String));
        }

        private void FrmSimphonyInterface_Load(object sender, EventArgs e)
        {
            if (CheckValidLicense() == false)
            {
                System.Windows.Forms.Application.Exit();
            }

            CreateTable();

            QueryString = "SELECT FilePathImportGL, FilePathImportFP FROM tblHistoryFilePath";
            DataTable tblHistoryFilePath = DBConnector.ReadTable(QueryString);
            if (tblHistoryFilePath.Rows.Count > 0)
            {
                this.txtImportGL.Text = tblHistoryFilePath.Rows[0][0].ToString().Trim();
                this.txtImportFP.Text = tblHistoryFilePath.Rows[0][1].ToString().Trim();
            }
        }

        private void BtnImportGL_Click(object sender, EventArgs e)
        {
            this.txtImportGL.Text = SelectFileFromFileDialog();
        }

        private void BtnImportFP_Click(object sender, EventArgs e)
        {
            this.txtImportFP.Text = SelectFileFromFileDialog();
        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            if (txtImportGL.Text.Trim() != string.Empty & this.txtImportFP.Text != string.Empty)
            {
                this.dtImportSource.Clear();
                ReadGLFile(this.txtImportGL.Text);
                ReadFPFile(this.txtImportFP.Text);
                CalculateTypeTotal(dtImportSource);
                this.btnExport.Enabled = true;

                QueryStringBuilder.AppendLine("DELETE FROM tblHistoryFilePath;");
                QueryStringBuilder.AppendLine("INSERT INTO tblHistoryFilePath(FilePathImportGL,FilePathImportFP) VALUES('" + this.txtImportGL.Text + "','" + this.txtImportFP.Text + "');");
                int _AffectedRow = DBConnector.ExecuteQuery(QueryStringBuilder.ToString());
            }
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {
            /* Ready RevenueCenter */
            //QueryString = "SELECT RVC,Q3RVCCode,Description FROM tblRevenueCenter ORDER BY RVC";
            DataTable dtRevenue = DBConnector.ReadTable(QueryRevenue);
            DataTable dtRevenueCenter = DBConnector.ReadTable(QueryRevenueCenter);
            /* Ready FixedPeriod */
            DataTable dtFixedPeriod = DBConnector.ReadTable(QueryFixedPeriod);
            /* Ready FixedPeriod */
            DataTable dtTenderMediaRVCInclude_and_MISC = DBConnector.ReadTable(QueryTenderMediaRVCInclude_and_MISC);
            int _RevenueCenterAnalysisCode = -1;
            int _FixedPeriodAnalysisCode = -1;
            int _TaxAnalysisCode = -1;
            int _TaxAnalysisCodeActive = 0;
            int _TenderMediaRVCInclude = 0;
            string _COARounding = string.Empty;
            string _COACoverCount = string.Empty;
            
            if (dtTenderMediaRVCInclude_and_MISC.Rows.Count > 0)
            {
                _TenderMediaRVCInclude = Convert.ToInt32(dtTenderMediaRVCInclude_and_MISC.Rows[0]["TenderMediaRVCInclude"].ToString());
                _RevenueCenterAnalysisCode = Convert.ToInt32(dtTenderMediaRVCInclude_and_MISC.Rows[0]["RevenueCenterAnalysisCode"].ToString());
                _FixedPeriodAnalysisCode = Convert.ToInt32(dtTenderMediaRVCInclude_and_MISC.Rows[0]["FixedPeriodAnalysisCode"].ToString());
                _TaxAnalysisCode = Convert.ToInt32(dtTenderMediaRVCInclude_and_MISC.Rows[0]["TaxAnalysisCode"].ToString());
                _TaxAnalysisCodeActive = Convert.ToInt32(dtTenderMediaRVCInclude_and_MISC.Rows[0]["TaxAnalysisCodeActive"].ToString());
                _COARounding = dtTenderMediaRVCInclude_and_MISC.Rows[0]["MISCRoundingAccountCode"].ToString().Trim();
                _COACoverCount = dtTenderMediaRVCInclude_and_MISC.Rows[0]["MISCCoverCountAccountCode"].ToString().Trim();
            }

            string ExportFilePath = SaveFileIntoFileDialog();
            StringBuilder fileContent = new StringBuilder();
            StringBuilder fileContent_Stat = new StringBuilder();

            fileContent_Stat.AppendLine("Transaction Date,Transaction Date,Details,Remark,Value,Chart Of Account,Analysis Code 1,Analysis Code 2,Analysis Code 3,Analysis Code 4,Analysis Code 5,Analysis Code 6,Analysis Code 7,Analysis Code 8,Analysis Code 9,Analysis Code 10,Journal Type");

            string _GLTrandate = string.Empty;
            var GLTranDate = (from p in dtImportSource.AsEnumerable()
                              where p.Field<string>("Type") == "GLID"

                              select new
                              {
                                  Value = p.Field<decimal>("Value"),
                              }).ToList();

            if (GLTranDate.Count > 0)
            {
                _GLTrandate = GLTranDate[0].Value.ToString();
            }

            if (_GLTrandate.Trim() == string.Empty)
            {
                MessageBox.Show("Invalid import file.", "Import File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            try
            {
                int _GLYear = Convert.ToInt16(_GLTrandate.Substring(0, 4));
                int _GLMonth = Convert.ToInt16(_GLTrandate.Substring(4, 2));
                int _GLDay = Convert.ToInt16(_GLTrandate.Substring(6, 2));
                DateTime _dtGenerate = new DateTime(_GLYear, _GLMonth, _GLDay);
                int result = DateTime.Compare(_dtGenerate, dtExpiry);

                if (result > 0)
                {
                    MessageBox.Show("Lisence had expired. Please contact support to renew license.", "License Expired", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Invalid import file.", "Import File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            #region Tender Media Data

            if (Convert.ToDecimal(this.txtTotalTenderMedia.Text) != 0)
            {
                //QueryString = "SELECT TenderMedia,AccountCode,Description FROM tblTenderMedia ORDER BY TenderMedia";
                DataTable dtTenderMedia = DBConnector.ReadTable(QueryTenderMedia);

                //Type,Code,Mapping,Value,AdditionalValue,MappingDescription
                var grpTenderMedia = (from p in dtImportSource.AsEnumerable()
                                      where p.Field<string>("Type") == "TND"
                                      group p by new
                                      {
                                          Type = p.Field<string>("Type"),
                                          Code = p.Field<string>("Code"),
                                          Mapping = p.Field<string>("Mapping"),
                                          AdditionalValue = p.Field<string>("AdditionalValue")
                                      } into grp
                                      join t in dtTenderMedia.AsEnumerable()
                                      on grp.Key.Mapping equals t.Field<string>("Tender Media") into eMapping
                                      from dMapping in eMapping.DefaultIfEmpty()
                                      join s in dtRevenueCenter.AsEnumerable()
                                      on grp.Key.Code equals s.Field<string>("RVC") into eMapAnalysisCode
                                      from dMapAnalysisCode in eMapAnalysisCode.DefaultIfEmpty()

                                      select new
                                      {
                                          Type = grp.Key.Type,
                                          Code = grp.Key.Code,
                                          Mapping = grp.Key.Mapping,
                                          AccountCode = dMapping == null ? "" : dMapping.Field<string>("Account Code"),
                                          Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                          AdditionalValue = grp.Key.AdditionalValue,
                                          MappingDescription = dMapping == null ? "" : dMapping.Field<string>("Description"),
                                          AnalysisCodeValue = dMapAnalysisCode == null ? "" : dMapAnalysisCode.Field<string>("Q3 RVC Code"),
                                      }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpTenderMedia)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = item.MappingDescription;
                    string _Amount = item.Amount.ToString("0.00");
                    string _ChartOfAccount = item.AccountCode;
                    string _A1 = string.Empty;
                    string _A2 = string.Empty;
                    string _A3 = string.Empty;
                    string _A4 = string.Empty;
                    string _A5 = string.Empty;
                    string _A6 = string.Empty;
                    string _A7 = string.Empty;
                    string _A8 = string.Empty;
                    string _A9 = string.Empty;
                    string _A10 = string.Empty;
                    string _JournalType = "POSRV";

                    if (isEmptyChartOfAccount(item.Type, item.Code, item.AccountCode))
                    {
                        return;
                    }

                    if (_TenderMediaRVCInclude == 1)
                    {
                        _A1 = MapAnalysisCode(_RevenueCenterAnalysisCode, 1, item.AnalysisCodeValue);
                        _A2 = MapAnalysisCode(_RevenueCenterAnalysisCode, 2, item.AnalysisCodeValue);
                        _A3 = MapAnalysisCode(_RevenueCenterAnalysisCode, 3, item.AnalysisCodeValue);
                        _A4 = MapAnalysisCode(_RevenueCenterAnalysisCode, 4, item.AnalysisCodeValue);
                        _A5 = MapAnalysisCode(_RevenueCenterAnalysisCode, 5, item.AnalysisCodeValue);
                        _A6 = MapAnalysisCode(_RevenueCenterAnalysisCode, 6, item.AnalysisCodeValue);
                        _A7 = MapAnalysisCode(_RevenueCenterAnalysisCode, 7, item.AnalysisCodeValue);
                        _A8 = MapAnalysisCode(_RevenueCenterAnalysisCode, 8, item.AnalysisCodeValue);
                        _A9 = MapAnalysisCode(_RevenueCenterAnalysisCode, 9, item.AnalysisCodeValue);
                        _A10 = MapAnalysisCode(_RevenueCenterAnalysisCode, 10, item.AnalysisCodeValue);
                    }

                    fileContent.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }

            #endregion Tender Media Data

            #region Revenue Data

            if (Convert.ToDecimal(this.txtTotalRevenue.Text) != 0)
            {
                /* Code = Rev Center
                /* Additional Value =  F.Period, then show on the Aanlysis Code we set on SETUP > F.Period */
                //DataTable dtRevenue = DBConnector.ReadTable(QueryRevenue);

                //Type,Code,Mapping,Value,AdditionalValue,MappingDescription
                var grpRevenue = (from p in dtImportSource.AsEnumerable()
                                  where p.Field<string>("Type") == "FPMI"
                                  group p by new
                                  {
                                      Type = p.Field<string>("Type"),
                                      Code = p.Field<string>("Code"),
                                      Mapping = p.Field<string>("Mapping"),
                                      AdditionalValue = p.Field<string>("AdditionalValue")
                                  } into grp
                                  join t in dtRevenue.AsEnumerable()
                                  on grp.Key.Mapping equals t.Field<string>("Major Group") into eMapping
                                  from dMapping in eMapping.DefaultIfEmpty()
                                  join s in dtRevenueCenter.AsEnumerable()
                                  on grp.Key.Code equals s.Field<string>("RVC") into eMapAnalysisCode
                                  from dMapAnalysisCode in eMapAnalysisCode.DefaultIfEmpty()

                                  select new
                                  {
                                      Type = grp.Key.Type,
                                      Code = grp.Key.Code,
                                      Mapping = grp.Key.Mapping,
                                      AccountCode = dMapping == null ? "" : dMapping.Field<string>("Account Code"),
                                      Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                      AdditionalValue = grp.Key.AdditionalValue,
                                      MappingDescription = dMapping == null ? "" : dMapping.Field<string>("Description"),
                                      AnalysisCodeValue = dMapAnalysisCode == null ? "" : dMapAnalysisCode.Field<string>("Q3 RVC Code"),
                                  }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpRevenue)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = item.MappingDescription;
                    string _Amount = (item.Amount * -1).ToString("0.00");
                    string _ChartOfAccount = item.AccountCode;
                    string _JournalType = "POSRV";

                    //Map Revenue Center Analysis Codes
                    string _A1 = MapAnalysisCode(_RevenueCenterAnalysisCode, 1, item.AnalysisCodeValue);
                    string _A2 = MapAnalysisCode(_RevenueCenterAnalysisCode, 2, item.AnalysisCodeValue);
                    string _A3 = MapAnalysisCode(_RevenueCenterAnalysisCode, 3, item.AnalysisCodeValue);
                    string _A4 = MapAnalysisCode(_RevenueCenterAnalysisCode, 4, item.AnalysisCodeValue);
                    string _A5 = MapAnalysisCode(_RevenueCenterAnalysisCode, 5, item.AnalysisCodeValue);
                    string _A6 = MapAnalysisCode(_RevenueCenterAnalysisCode, 6, item.AnalysisCodeValue);
                    string _A7 = MapAnalysisCode(_RevenueCenterAnalysisCode, 7, item.AnalysisCodeValue);
                    string _A8 = MapAnalysisCode(_RevenueCenterAnalysisCode, 8, item.AnalysisCodeValue);
                    string _A9 = MapAnalysisCode(_RevenueCenterAnalysisCode, 9, item.AnalysisCodeValue);
                    string _A10 = MapAnalysisCode(_RevenueCenterAnalysisCode, 10, item.AnalysisCodeValue);

                    //Map Fixed Period Analysis Codes
                    _A1 = _A1.Trim() != string.Empty ? _A1 : MapFixedPeriod(_FixedPeriodAnalysisCode, 1, item.AdditionalValue, dtFixedPeriod);
                    _A2 = _A2.Trim() != string.Empty ? _A2 : MapFixedPeriod(_FixedPeriodAnalysisCode, 2, item.AdditionalValue, dtFixedPeriod);
                    _A3 = _A3.Trim() != string.Empty ? _A3 : MapFixedPeriod(_FixedPeriodAnalysisCode, 3, item.AdditionalValue, dtFixedPeriod);
                    _A4 = _A4.Trim() != string.Empty ? _A4 : MapFixedPeriod(_FixedPeriodAnalysisCode, 4, item.AdditionalValue, dtFixedPeriod);
                    _A5 = _A5.Trim() != string.Empty ? _A5 : MapFixedPeriod(_FixedPeriodAnalysisCode, 5, item.AdditionalValue, dtFixedPeriod);
                    _A6 = _A6.Trim() != string.Empty ? _A6 : MapFixedPeriod(_FixedPeriodAnalysisCode, 6, item.AdditionalValue, dtFixedPeriod);
                    _A7 = _A7.Trim() != string.Empty ? _A7 : MapFixedPeriod(_FixedPeriodAnalysisCode, 7, item.AdditionalValue, dtFixedPeriod);
                    _A8 = _A8.Trim() != string.Empty ? _A8 : MapFixedPeriod(_FixedPeriodAnalysisCode, 8, item.AdditionalValue, dtFixedPeriod);
                    _A9 = _A9.Trim() != string.Empty ? _A9 : MapFixedPeriod(_FixedPeriodAnalysisCode, 9, item.AdditionalValue, dtFixedPeriod);
                    _A10 = _A10.Trim() != string.Empty ? _A10 : MapFixedPeriod(_FixedPeriodAnalysisCode, 10, item.AdditionalValue, dtFixedPeriod);

                    if (isEmptyChartOfAccount(item.Type, item.Code, item.AccountCode))
                    {
                        return;
                    }

                    if (String.Concat(_A1, _A2, _A3, _A4, _A5, _A6, _A7, _A8, _A9, _A10).Trim() == string.Empty)
                    {
                        MessageBox.Show("Analysis code is empty when refer Settings > Fixed Period" + Environment.NewLine + "Please setup the valid info on Fixe Period", "Error Export File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    fileContent.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }

            #endregion Revenue Data

            #region Discount

            if (Convert.ToDecimal(this.txtTotalDiscount.Text) != 0)
            {
                //Discount, AccountCode AS [Account Code], Description FROM tblDiscount
                DataTable tblDiscount = DBConnector.ReadTable(QueryDiscount);

                //Type,Code,Mapping,Value,AdditionalValue,MappingDescription
                var grpDiscount = (from p in dtImportSource.AsEnumerable()
                                   where p.Field<string>("Type") == "DSC"
                                   group p by new
                                   {
                                       Type = p.Field<string>("Type"),
                                       Code = p.Field<string>("Code"),
                                       Mapping = p.Field<string>("Mapping"),
                                       AdditionalValue = p.Field<string>("AdditionalValue")
                                   } into grp
                                   join t in tblDiscount.AsEnumerable()
                                   on grp.Key.Mapping equals t.Field<string>("Discount") into eMapping
                                   from dMapping in eMapping.DefaultIfEmpty()

                                   select new
                                   {
                                       Type = grp.Key.Type,
                                       Code = grp.Key.Code,
                                       Mapping = grp.Key.Mapping,
                                       AccountCode = dMapping == null ? "" : dMapping.Field<string>("Account Code"),
                                       Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                       AdditionalValue = grp.Key.AdditionalValue,
                                       MappingDescription = dMapping == null ? "" : dMapping.Field<string>("Description"),
                                   }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpDiscount)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = item.MappingDescription;
                    string _Amount = (item.Amount * -1).ToString("0.00");
                    string _ChartOfAccount = item.AccountCode;
                    string _JournalType = "POSRV";

                    //Map Revenue Center Analysis Codes
                    string _A1 = string.Empty;
                    string _A2 = string.Empty;
                    string _A3 = string.Empty;
                    string _A4 = string.Empty;
                    string _A5 = string.Empty;
                    string _A6 = string.Empty;
                    string _A7 = string.Empty;
                    string _A8 = string.Empty;
                    string _A9 = string.Empty;
                    string _A10 = string.Empty;

                    if (isEmptyChartOfAccount(item.Type, item.Code, item.AccountCode))
                    {
                        return;
                    }

                    fileContent.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }

            #endregion Discount

            #region Tax
            if (Convert.ToDecimal(this.txtTotalTax.Text) != 0)
            {
                //Discount, AccountCode AS [Account Code], Description FROM tblDiscount
                DataTable tblDiscount = DBConnector.ReadTable(QueryTax);

                //TaxID AS [Tax ID], TaxCode AS [Tax Code], Description
                var grpDiscount = (from p in dtImportSource.AsEnumerable()
                                   where p.Field<string>("Type") == "TAX"
                                   group p by new
                                   {
                                       Type = p.Field<string>("Type"),
                                       Code = p.Field<string>("Code"),
                                       Mapping = p.Field<string>("Mapping"),
                                       AdditionalValue = p.Field<string>("AdditionalValue")
                                   } into grp
                                   join t in tblDiscount.AsEnumerable()
                                   on grp.Key.Mapping equals t.Field<string>("Tax ID") into eMapping
                                   from dMapping in eMapping.DefaultIfEmpty()

                                   select new
                                   {
                                       Type = grp.Key.Type,
                                       Code = grp.Key.Code,
                                       Mapping = grp.Key.Mapping,
                                       AccountCode = dMapping == null ? "" : dMapping.Field<string>("Account Code"),
                                       Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                       AnalysisCodeValue = dMapping == null ? "" : dMapping.Field<string>("Tax Code"),
                                       MappingDescription = dMapping == null ? "" : dMapping.Field<string>("Description"),
                                   }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpDiscount)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = item.MappingDescription;
                    string _Amount = (item.Amount * -1).ToString("0.00");
                    string _ChartOfAccount = item.AccountCode;
                    string _JournalType = "POSRV";

                    //Map Revenue Center Analysis Codes
                    string _A1 = string.Empty;
                    string _A2 = string.Empty;
                    string _A3 = string.Empty;
                    string _A4 = string.Empty;
                    string _A5 = string.Empty;
                    string _A6 = string.Empty;
                    string _A7 = string.Empty;
                    string _A8 = string.Empty;
                    string _A9 = string.Empty;
                    string _A10 = string.Empty;

                    if (_TaxAnalysisCodeActive == 1)
                    {
                         _A1 = MapAnalysisCode(_TaxAnalysisCode, 1, item.AnalysisCodeValue);
                         _A2 = MapAnalysisCode(_TaxAnalysisCode, 2, item.AnalysisCodeValue);
                         _A3 = MapAnalysisCode(_TaxAnalysisCode, 3, item.AnalysisCodeValue);
                         _A4 = MapAnalysisCode(_TaxAnalysisCode, 4, item.AnalysisCodeValue);
                         _A5 = MapAnalysisCode(_TaxAnalysisCode, 5, item.AnalysisCodeValue);
                         _A6 = MapAnalysisCode(_TaxAnalysisCode, 6, item.AnalysisCodeValue);
                         _A7 = MapAnalysisCode(_TaxAnalysisCode, 7, item.AnalysisCodeValue);
                         _A8 = MapAnalysisCode(_TaxAnalysisCode, 8, item.AnalysisCodeValue);
                         _A9 = MapAnalysisCode(_TaxAnalysisCode, 9, item.AnalysisCodeValue);
                         _A10 = MapAnalysisCode(_TaxAnalysisCode, 10, item.AnalysisCodeValue);
                    }

                    if (isEmptyChartOfAccount(item.Type, item.Code, item.AccountCode))
                    {
                        return;
                    }

                    fileContent.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }
            #endregion Tax

            #region Service Charges

            if (Convert.ToDecimal(this.txtTotalServiceCharge.Text) != 0)
            {
                // ServiceCharge AS [Service Charge], AccountCode AS [Account Code], Description FROM tblServiceCharge
                DataTable tblServiceCharge = DBConnector.ReadTable(QueryServiceCharge);

                //Type,Code,Mapping,Value,AdditionalValue,MappingDescription
                var grpServiceCharge = (from p in dtImportSource.AsEnumerable()
                                   where p.Field<string>("Type") == "SVC"
                                   group p by new
                                   {
                                       Type = p.Field<string>("Type"),
                                       Code = p.Field<string>("Code"),
                                       Mapping = p.Field<string>("Mapping"),
                                       AdditionalValue = p.Field<string>("AdditionalValue")
                                   } into grp
                                   join t in tblServiceCharge.AsEnumerable()
                                   on grp.Key.Mapping equals t.Field<string>("Service Charge") into eMapping
                                   from dMapping in eMapping.DefaultIfEmpty()

                                   select new
                                   {
                                       Type = grp.Key.Type,
                                       Code = grp.Key.Code,
                                       Mapping = grp.Key.Mapping,
                                       AccountCode = dMapping == null ? "" : dMapping.Field<string>("Account Code"),
                                       Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                       AdditionalValue = grp.Key.AdditionalValue,
                                       MappingDescription = dMapping == null ? "" : dMapping.Field<string>("Description"),
                                   }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpServiceCharge)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = item.MappingDescription;
                    string _Amount = (item.Amount * -1).ToString("0.00");
                    string _ChartOfAccount = item.AccountCode;
                    string _JournalType = "POSRV";

                    //Map Revenue Center Analysis Codes
                    string _A1 = string.Empty;
                    string _A2 = string.Empty;
                    string _A3 = string.Empty;
                    string _A4 = string.Empty;
                    string _A5 = string.Empty;
                    string _A6 = string.Empty;
                    string _A7 = string.Empty;
                    string _A8 = string.Empty;
                    string _A9 = string.Empty;
                    string _A10 = string.Empty;

                    if (isEmptyChartOfAccount(item.Type, item.Code, item.AccountCode))
                    {
                        return;
                    }

                    fileContent.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }

            #endregion Service Charges

            #region Rounding

            if (Convert.ToDecimal(this.txtTotalRounding.Text) != 0)
            {
                var grpRounding = (from p in dtImportSource.AsEnumerable()
                                        where p.Field<string>("Type") == "SUM"
                                        group p by new
                                        {
                                            Type = p.Field<string>("Type"),
                                            Code = p.Field<string>("Code"),
                                        } into grp

                                        select new
                                        {
                                            Type = grp.Key.Type,
                                            Code = grp.Key.Code,
                                            Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                        }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpRounding)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = "Rounding";
                    string _Amount = (item.Amount * -1).ToString("0.00");
                    string _ChartOfAccount = _COARounding;
                    string _JournalType = "POSRV";

                    //Map Revenue Center Analysis Codes
                    string _A1 = string.Empty;
                    string _A2 = string.Empty;
                    string _A3 = string.Empty;
                    string _A4 = string.Empty;
                    string _A5 = string.Empty;
                    string _A6 = string.Empty;
                    string _A7 = string.Empty;
                    string _A8 = string.Empty;
                    string _A9 = string.Empty;
                    string _A10 = string.Empty;

                    if (_COARounding == string.Empty)
                    {
                        MessageBox.Show("Rounding chart of account is empty" + Environment.NewLine + "Please setup chart of account on rounding account", "Error Export File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    fileContent.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }

            #endregion Rounding

            #region Cover Count

            if (Convert.ToDecimal(this.txtTotalCoverCount.Text) != 0)
            {
                var grpCoverCount = (from p in dtImportSource.AsEnumerable()
                                  where p.Field<string>("Type") == "FPSUM"
                                  group p by new
                                  {
                                      Type = p.Field<string>("Type"),
                                      Code = p.Field<string>("Code"),
                                      AdditionalValue = p.Field<string>("AdditionalValue")
                                  } into grp
                                     join s in dtRevenueCenter.AsEnumerable()
                                  on grp.Key.Code equals s.Field<string>("RVC") into eMapAnalysisCode
                                     from dMapAnalysisCode in eMapAnalysisCode.DefaultIfEmpty()

                                     select new
                                  {
                                      Type = grp.Key.Type,
                                      Code = grp.Key.Code,
                                      Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                                      AdditionalValue = grp.Key.AdditionalValue,
                                      AnalysisCodeValue = dMapAnalysisCode == null ? "" : dMapAnalysisCode.Field<string>("Q3 RVC Code"),
                                     }).ToList();

                //ExportFile Columns: DocumentDate,PostingDate,ReferenceNumber,Remarks,Amount,ChartOfAccounts,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,JournalType
                foreach (var item in grpCoverCount)
                {
                    string _ReferenceNumber = "POS" + _GLTrandate;
                    string _Remarks = "Cover Count";
                    string _Amount = Math.Abs(item.Amount).ToString("0.00");
                    string _ChartOfAccount = _COACoverCount;
                    string _JournalType = "POSRV";

                    //Map Revenue Center Analysis Codes
                    string _A1 = MapAnalysisCode(_RevenueCenterAnalysisCode, 1, item.AnalysisCodeValue);
                    string _A2 = MapAnalysisCode(_RevenueCenterAnalysisCode, 2, item.AnalysisCodeValue);
                    string _A3 = MapAnalysisCode(_RevenueCenterAnalysisCode, 3, item.AnalysisCodeValue);
                    string _A4 = MapAnalysisCode(_RevenueCenterAnalysisCode, 4, item.AnalysisCodeValue);
                    string _A5 = MapAnalysisCode(_RevenueCenterAnalysisCode, 5, item.AnalysisCodeValue);
                    string _A6 = MapAnalysisCode(_RevenueCenterAnalysisCode, 6, item.AnalysisCodeValue);
                    string _A7 = MapAnalysisCode(_RevenueCenterAnalysisCode, 7, item.AnalysisCodeValue);
                    string _A8 = MapAnalysisCode(_RevenueCenterAnalysisCode, 8, item.AnalysisCodeValue);
                    string _A9 = MapAnalysisCode(_RevenueCenterAnalysisCode, 9, item.AnalysisCodeValue);
                    string _A10 = MapAnalysisCode(_RevenueCenterAnalysisCode, 10, item.AnalysisCodeValue);

                    //Map Fixed Period Analysis Codes
                    _A1 = _A1.Trim() != string.Empty ? _A1 : MapFixedPeriod(_FixedPeriodAnalysisCode, 1, item.AdditionalValue, dtFixedPeriod);
                    _A2 = _A2.Trim() != string.Empty ? _A2 : MapFixedPeriod(_FixedPeriodAnalysisCode, 2, item.AdditionalValue, dtFixedPeriod);
                    _A3 = _A3.Trim() != string.Empty ? _A3 : MapFixedPeriod(_FixedPeriodAnalysisCode, 3, item.AdditionalValue, dtFixedPeriod);
                    _A4 = _A4.Trim() != string.Empty ? _A4 : MapFixedPeriod(_FixedPeriodAnalysisCode, 4, item.AdditionalValue, dtFixedPeriod);
                    _A5 = _A5.Trim() != string.Empty ? _A5 : MapFixedPeriod(_FixedPeriodAnalysisCode, 5, item.AdditionalValue, dtFixedPeriod);
                    _A6 = _A6.Trim() != string.Empty ? _A6 : MapFixedPeriod(_FixedPeriodAnalysisCode, 6, item.AdditionalValue, dtFixedPeriod);
                    _A7 = _A7.Trim() != string.Empty ? _A7 : MapFixedPeriod(_FixedPeriodAnalysisCode, 7, item.AdditionalValue, dtFixedPeriod);
                    _A8 = _A8.Trim() != string.Empty ? _A8 : MapFixedPeriod(_FixedPeriodAnalysisCode, 8, item.AdditionalValue, dtFixedPeriod);
                    _A9 = _A9.Trim() != string.Empty ? _A9 : MapFixedPeriod(_FixedPeriodAnalysisCode, 9, item.AdditionalValue, dtFixedPeriod);
                    _A10 = _A10.Trim() != string.Empty ? _A10 : MapFixedPeriod(_FixedPeriodAnalysisCode, 10, item.AdditionalValue, dtFixedPeriod);

                    if (isEmptyChartOfAccount(item.Type, item.Code, _COACoverCount))
                    {
                        return;
                    }

                    if (String.Concat(_A1, _A2, _A3, _A4, _A5, _A6, _A7, _A8, _A9, _A10).Trim() == string.Empty)
                    {
                        MessageBox.Show("Analysis code is empty when refer Settings > Fixed Period" + Environment.NewLine + "Please setup the valid info on Fixe Period", "Error Export File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    fileContent_Stat.AppendLine(_GLTrandate + "," + _GLTrandate + "," + _ReferenceNumber + "," + _Remarks + "," + _Amount + "," + _ChartOfAccount + "," + _A1 + "," + _A2 + "," + _A3 + "," + _A4 + "," + _A5 + "," + _A6 + "," + _A7 + "," + _A8 + "," + _A9 + "," + _A10 + "," + _JournalType + ",,,");
                }
            }

            #endregion Cover Count

            if (ExportFilePath.Trim() != string.Empty)
            {
                string ExportFilePath_Stat = ExportFilePath.Substring(0, ExportFilePath.Length - 4);
                if (File.Exists(ExportFilePath_Stat))
                {
                    File.Delete(ExportFilePath_Stat);
                }
                File.WriteAllText(ExportFilePath, fileContent.ToString());
                string _ExportFilePath_Stat = ExportFilePath_Stat + "_stat.csv";
                File.WriteAllText(_ExportFilePath_Stat, fileContent_Stat.ToString()); 
                MessageBox.Show("Standard File export to " + ExportFilePath + Environment.NewLine +  Environment.NewLine + "Statistic File export to " + _ExportFilePath_Stat, "Export to file successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool CheckValidLicense()
        {
            DateTime _dtToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            int result = DateTime.Compare(_dtToday, dtExpiry);

            if (result > 0)
            {
                MessageBox.Show("Lisence had expired. Please contact support to renew license.", "License Expired", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else
            {
                int diffDays = Convert.ToInt32(dtExpiry.Subtract(DateTime.Today).TotalDays);
                if (diffDays <= 30)
                {
                    MessageBox.Show("Lisence going to expired in " + diffDays.ToString() + " days. Please contact support to renew license.", "License Expired", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            return true;
        }

        private void CreateTable()
        {
            // Create tblTenderMedia
            SQLiteConnection sqlite_con = new SQLiteConnection("Data Source=database.db; Version = 3; New = True; Compress = True; ");
            sqlite_con.Open();
            SQLiteCommand commandSQLite = sqlite_con.CreateCommand();
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblTenderMedia'";
            var _tblTenderMedia = commandSQLite.ExecuteScalar();
            if (_tblTenderMedia == null)
            {
                string Createsql = "CREATE TABLE tblTenderMedia(TenderMedia TEXT, AccountCode TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblRevenue
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblRevenue'";
            var _tblRevenue = commandSQLite.ExecuteScalar();
            if (_tblRevenue == null)
            {
                string Createsql = "CREATE TABLE tblRevenue(MajorGroup TEXT, AccountCode TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblDiscount
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblDiscount'";
            var tblDiscount = commandSQLite.ExecuteScalar();
            if (tblDiscount == null)
            {
                string Createsql = "CREATE TABLE tblDiscount(Discount TEXT, AccountCode TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblTax
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblTax'";
            var tblTax = commandSQLite.ExecuteScalar();
            if (tblTax == null)
            {
                string Createsql = "CREATE TABLE tblTax(TaxID TEXT, AccountCode TEXT, TaxCode TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblServiceCharge
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblServiceCharge'";
            var tblServiceCharge = commandSQLite.ExecuteScalar();
            if (tblServiceCharge == null)
            {
                string Createsql = "CREATE TABLE tblServiceCharge(ServiceCharge TEXT, AccountCode TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblFixedPeriod
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblFixedPeriod'";
            var tblFixedPeriod = commandSQLite.ExecuteScalar();
            if (tblFixedPeriod == null)
            {
                string Createsql = "CREATE TABLE tblFixedPeriod(FixedPeriodFrom TEXT, FixedPeriodTo TEXT, Q3ServingPeriod TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblRevenueCenter
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblRevenueCenter'";
            var tblRevenueCenter = commandSQLite.ExecuteScalar();
            if (tblRevenueCenter == null)
            {
                string Createsql = "CREATE TABLE tblRevenueCenter(RVC TEXT, Q3RVCCode TEXT, Description TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            // Create tblTenderMediaRVCInclude_and_MISC
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblTenderMediaRVCInclude_and_MISC'";
            var tblTenderMediaRVCInclude_and_MISC = commandSQLite.ExecuteScalar();
            if (tblTenderMediaRVCInclude_and_MISC == null)
            {
                string Createsql = "CREATE TABLE tblTenderMediaRVCInclude_and_MISC(TenderMediaRVCInclude INTEGER,TaxAnalysisCode INTEGER,TaxAnalysisCodeActive INTEGER,FixedPeriodAnalysisCode INTEGER,RevenueCenterAnalysisCode INTEGER,MISCRoundingAccountCode TEXT,MISCCoverCountAccountCode TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();

                QueryStringBuilder.AppendLine("INSERT INTO tblTenderMediaRVCInclude_and_MISC(TenderMediaRVCInclude,TaxAnalysisCode,TaxAnalysisCodeActive,FixedPeriodAnalysisCode,RevenueCenterAnalysisCode,MISCRoundingAccountCode,MISCCoverCountAccountCode) VALUES(0,0,0,0,0,'','');");
                int _AffectedRow = SQLLiteDB.DBConnector.ExecuteQuery(QueryStringBuilder.ToString());
            }

            // Create tblHistoryFilePath
            commandSQLite.CommandText = "SELECT name FROM sqlite_master WHERE name='tblHistoryFilePath'";
            var tblHistoryFilePath = commandSQLite.ExecuteScalar();
            if (tblHistoryFilePath == null)
            {
                string Createsql = "CREATE TABLE tblHistoryFilePath(FilePathImportGL TEXT, FilePathImportFP TEXT)";
                commandSQLite = sqlite_con.CreateCommand();
                commandSQLite.CommandText = Createsql;
                commandSQLite.ExecuteNonQuery();
            }

            sqlite_con.Close();
        }

        private string SelectFileFromFileDialog()
        {
            string _FilePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|csv files (*.csv)|*.csv";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    _FilePath = openFileDialog.FileName;
                }
            }
            return _FilePath;
        }

        private string SaveFileIntoFileDialog()
        {
            string _FilePath = string.Empty;

            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                saveFileDialog.Filter = "csv files (*.csv)|*.csv";
                saveFileDialog.FilterIndex = 1;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    _FilePath = saveFileDialog.FileName;
                }
            }
            return _FilePath;
        }

        private void ReadGLFile(string FilePath)
        {
            var fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            var sr = new StreamReader(fs, Encoding.UTF8);

            string line = String.Empty;

            while ((line = sr.ReadLine()) != null)
            {
                string col1 = string.Empty;
                string col2 = string.Empty;
                string col3 = string.Empty;
                string col5 = string.Empty;
                string col6 = string.Empty;
                string col24 = string.Empty;
                string col30 = string.Empty;

                /*
                string col13 = string.Empty;
                string col14 = string.Empty;
                string col30 = string.Empty;
                string col40 = string.Empty;
                string col41 = string.Empty;
                */

                int colIndex = 0;
                foreach (string ColumnValue in line.Split('|'))
                {
                    switch (colIndex)
                    {
                        case 0:
                            col1 = ColumnValue;
                            break;

                        case 1:
                            col2 = ColumnValue;
                            break;

                        case 2:
                            col3 = ColumnValue;
                            break;

                        case 4:
                            col5 = ColumnValue;
                            break;

                        case 5:
                            col6 = ColumnValue;
                            break;

                        case 29:
                            col30 = ColumnValue;
                            break;

                        default:
                            break;
                    }
                    colIndex++;
                }

                Decimal Value = 0;
                bool isDecimal = false;
                if (col1.ToString().ToUpper() == "GLID")
                {
                    isDecimal = true;
                    Value = Convert.ToInt32(col6);
                }
                if (col1.ToString().ToUpper() == "TND" || col1.ToString().ToUpper() == "TAX")
                {
                    isDecimal = Decimal.TryParse(col6, out Value);
                }
                else if (col1.ToString().ToUpper() == "DSC" || col1.ToString().ToUpper() == "SVC")
                {
                    isDecimal = Decimal.TryParse(col5, out Value);
                }
                else if (col1.ToString().ToUpper() == "SUM")
                {
                    isDecimal = Decimal.TryParse(col30, out Value);
                }

                if (isDecimal)
                {
                    dtImportSource.Rows.Add(col1, col2, col3, Value);
                }
            }
        }

        private void ReadFPFile(string FilePath)
        {
            var fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            var sr = new StreamReader(fs, Encoding.UTF8);

            string line = String.Empty;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.Substring(0, 5).ToUpper() == "FPSUM" || line.Substring(0, 4).ToUpper() == "FPMI")
                {
                    string col1 = string.Empty;
                    string col3 = string.Empty;
                    string col13 = string.Empty;
                    string col14 = string.Empty;
                    string col24 = string.Empty;
                    string col40 = string.Empty;
                    string col41 = string.Empty;

                    int colIndex = 0;
                    foreach (string ColumnValue in line.Split('|'))
                    {
                        switch (colIndex)
                        {
                            case 0:
                                col1 = ColumnValue;
                                break;

                            case 2:
                                col3 = ColumnValue;
                                break;

                            case 12:
                                col13 = ColumnValue;
                                break;

                            case 13:
                                col14 = ColumnValue;
                                break;

                            case 23:
                                col24 = ColumnValue;
                                break;

                            case 39:
                                col40 = ColumnValue;
                                break;

                            case 40:
                                col41 = ColumnValue;
                                break;

                            default:
                                break;
                        }
                        colIndex++;
                    }
                    Decimal Value = 0;
                    bool isDecimal = false;
                    string AdditionalValue = string.Empty;
                    if (col1.ToString().ToUpper() == "FPSUM")
                    {
                        isDecimal = Decimal.TryParse(col14, out Value);
                        AdditionalValue = col41;
                    }
                    else if (col1.ToString().ToUpper() == "FPMI")
                    {
                        isDecimal = Decimal.TryParse(col24, out Value);
                        AdditionalValue = col40;
                    }

                    if (isDecimal)
                    {
                        dtImportSource.Rows.Add(col1, col3, col13, Value, AdditionalValue);
                    }
                }
            }
        }

        private void CalculateTypeTotal(DataTable dtSource)
        {
            var result = (from row in dtImportSource.AsEnumerable()
                          group row by new
                          {
                              Type = row.Field<string>("Type"),
                              //MatchingCode = row.Field<string>("MatchingCode"),
                          } into grp
                          select new
                          {
                              Type = grp.Key.Type,
                              //MatchingCode = grp.Key.MatchingCode,
                              Amount = grp.Sum(r => r.Field<Decimal>("Value")),
                          }).ToList();

            foreach (var item in result)
            {
                switch (item.Type.ToString().ToUpper())
                {
                    case "TND":
                        this.txtTotalTenderMedia.Text = (Convert.ToDecimal(item.Amount) * -1).ToString("N2");
                        break;

                    case "FPMI":
                        this.txtTotalRevenue.Text = item.Amount.ToString("N2");
                        break;

                    case "DSC":
                        this.txtTotalDiscount.Text = item.Amount.ToString("N2");
                        break;

                    case "TAX":
                        this.txtTotalTax.Text = item.Amount.ToString("N2");
                        break;

                    case "SVC":
                        this.txtTotalServiceCharge.Text = item.Amount.ToString("N2");
                        break;

                    case "SUM":
                        this.txtTotalRounding.Text = item.Amount.ToString("N2");
                        break;

                    case "FPSUM":
                        this.txtTotalCoverCount.Text = item.Amount.ToString("N2");
                        break;

                    default:
                        break;
                }
            }

            decimal _Discount = Math.Abs(Convert.ToDecimal(this.txtTotalDiscount.Text));
            decimal _ServiceCharges = Math.Abs(Convert.ToDecimal(this.txtTotalServiceCharge.Text));
            decimal _TenderMedia = Math.Abs(Convert.ToDecimal(this.txtTotalTenderMedia.Text));
            decimal _Tax = Math.Abs(Convert.ToDecimal(this.txtTotalTax.Text));
            decimal _Revenue = Math.Abs(Convert.ToDecimal(this.txtTotalRevenue.Text));
            decimal _Rounding = Convert.ToDecimal(this.txtTotalRounding.Text);

            decimal _Variance = _ServiceCharges - _TenderMedia + _Tax + _Revenue + _Rounding - _Discount;
            this.txtTotalVariance.Text = _Variance.ToString("N2");
        }

        private bool isEmptyChartOfAccount(string Type, string Code, string ChartOfAccount)
        {
            bool _isEmptyChartOfAccount = false;
            if (ChartOfAccount.Trim() == string.Empty)
            {
                MessageBox.Show(Type + " chart of account is empty on code " + Code + Environment.NewLine + "Please setup chart of account on " + Type, "Error Export File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _isEmptyChartOfAccount = true;
            }
            return _isEmptyChartOfAccount;
        }

        private string MapAnalysisCode(int SetupAnalysisCode, int SelectedAnalysisCode, string AnalysisCodeValue)
        {
            string _MapAnalysisCode = string.Empty;
            if (SetupAnalysisCode + 1 == SelectedAnalysisCode)
            {
                _MapAnalysisCode = AnalysisCodeValue;
            }
            return _MapAnalysisCode;
        }

        private string MapFixedPeriod(int SetupAnalysisCode, int SelectedAnalysisCode, string AdditionalValue, DataTable DataTableFixedPeriod)
        {
            int _AdditionalValue = Convert.ToInt32(AdditionalValue);

            string _MapFixedPeriod = string.Empty;
            if (SetupAnalysisCode + 1 == SelectedAnalysisCode)
            {
                foreach (DataRow dr in DataTableFixedPeriod.Rows)
                {
                    int _FixedPeriodFrom = Convert.ToInt32(dr[0]);
                    int _FixedPeriodTo = Convert.ToInt32(dr[1]);

                    if (_AdditionalValue >= _FixedPeriodFrom && _AdditionalValue <= _FixedPeriodTo)
                    {
                        _MapFixedPeriod = dr[2].ToString();
                    }
                }
            }
            return _MapFixedPeriod;
        }

        private string CsvEscape(string value)
        {
            if (value.Contains(","))
            {
                return "\"" + value.Replace("\"", "\"\"") + "\"";
            }
            return value;
        }

        private void TsmiSettings_Click(object sender, EventArgs e)
        {
            frmSettings frmSettings = new frmSettings();
            frmSettings.StartPosition = FormStartPosition.CenterParent;
            frmSettings.ShowDialog();
        }

        private void TsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsmiAbout_Click(object sender, EventArgs e)
        {
            frmAbout frmAbout = new frmAbout(dtExpiry);
            frmAbout.StartPosition = FormStartPosition.CenterParent;
            frmAbout.ShowDialog(); 
        }
    }
}